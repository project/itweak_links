<?php

/**
 * @file
 * Holds the contents of a preprocess function moved into its own file
 * to ease memory requirements and having too much code in one file.
 */

/**
 * Helper function to compare arrays.
 */
function _itweak_links_link_cmp($a, $b) {
    if ($a['weight'] == $b['weight']) {
        return 0;
    }
    return ($a['weight'] < $b['weight']) ? -1 : 1;
}

/*
 * This is the place for customization. 
 * Currently hard-coded, but can be developed into admin input feature.
 */
function _itweak_links_preprocess_links_settings() {
  static $return = '';
  if (!$return) {
    $settings = array(
    // This hash array lists all the links we want to modify in their desired order
    // The array is keyed by the match pattern.
    // The array elements are hash arrays of options.
    // Options array have mandatory 'mode' and a number of optional elements.
    // 'mode' is one of:
    // - 'link' (default) - leave as link,
    // - 'none'           - remove completely,
    // - 'cssbutton'      - format as a CSS button / or image replacement (with <span> tag insertion),
    // - 'dropmenu'       - place into drop-down menu (FIXME: NOT IMPLEMENTED)
    // optional 'key' changes what to match the pattern to, e.g. 'key'=>'href' will check the link's href instead of its name
    // If 'weight' is not given, all listed links are moved to the end of the list.
    // If 'weight' is given, it moves the weight of that item out of default 
    //  order - most usefull to put the link ahead of all other links (negative weight).
    // Note that CSS can float:right some links, so their order will appear reverse to the one given here.
    // optional 'class_replace' element defines CSS class to be used for the link instead of the original.
    // optional 'class' element defines additional CSS class for the link., ignored if 'class_replace' is given
    // optional 'prefix' and/or 'suffix' element defines HTML code to add before/after link title, $link->html is set to TRUE then.
    // For 'mode' => 'dropdown', optional 'dropdown_class' element defines CSS class for the drop-down div. 
    //   If not given, 'dropdown_class' defaults to 'itweak-links-dropdown'
    // 'title' will replace link title
    // 'type' and 'node_type' restrict matching based on $type and $node_type given by the caller

      // #BEGIN# Forum Primary & Secondary links
      'forum'          => array('mode' => 'cssbutton', 'class' => 'itweak-links-cssbutton'),
      'mark-read'      => array('mode' => 'cssbutton', 'class' => 'itweak-links-cssbutton', 'title' => t('Mark all read'), ),
      'new-posts'      => array('mode' => 'link', ),
      'my-posts'       => array('mode' => 'link', ),
      'unanswered'     => array('mode' => 'link', 'title' => t('Unanswered'), ),
      'active'         => array('mode' => 'link', ),
      'login'          => array('mode' => 'link', ),
      // #END# Forum Primary & Secondary links

      // #BEGIN# Forum node/comment links
      'statistics_counter' => array('mode' => 'link', ),

      // Links by various modules

      'flag-bookmarks' => array('mode' => 'link', ),
      'fasttoggle_%' => array('mode' => 'dropdown',
        'dropdown_class' => 'itweak-links-dropdown',
        'dropdown_name' => 'manage',
        'class' => '', // link_class
      ),

      // "Subscribe" links by notifications.module
//      'notifications/subscribe/%/thread/nid/' => array('key' => 'href', 'mode' => 'none', 'type' => 'nodecomment'), // Remove all notification links for replies to the comment. FIXME: Need 'Subscribe to: comments' notification?
//      'notifications/subscribe/%/nodetype/' => array('key' => 'href', 'mode' => 'none'), // Remove all notification links for the node type
//      'notifications/subscribe/%' => array('key' => 'href', 'mode' => 'link', ),
      'notifications/%subscribe/%' => array('key' => 'href', 'mode' => 'dropdown',
        'dropdown_class' => 'itweak-links-dropdown',
        'dropdown_name' => 'subscribe',
        'class' => '', // link_class
      ),

      // mollom.module
      'mollom_report' => array('mode' => 'link', 'title' => t('spam')),

      // AF & Core Forum links
      'reply-allowed'  => array('mode' => 'cssbutton', 'class' => 'itweak-links-cssbutton', ),
      'comment_add'    => array('mode' => 'cssbutton', 'class' => 'itweak-links-cssbutton', 'title' => t('reply'), ),
      'comment_reply'  => array('mode' => 'cssbutton', 'class' => 'itweak-links-cssbutton', 'title' => t('reply'), ),
      'quote'          => array('mode' => 'cssbutton', 'class' => 'itweak-links-cssbutton', 'title' => t('quote'), ),
      'comment_edit'   => array('mode' => 'cssbutton', 'class' => 'itweak-links-cssbutton', 'title' => t('edit'),  ),
      'post_edit'      => array('mode' => 'cssbutton', 'class' => 'itweak-links-cssbutton', 'title' => t('edit'),  ),
      'comment_delete' => array('mode' => 'cssbutton', 'class' => 'itweak-links-cssbutton', 'title' => t('delete'), ),
      'post_delete'    => array('mode' => 'cssbutton', 'class' => 'itweak-links-cssbutton', 'title' => t('delete'), ),

      // #END# Forum node/comment links

      // Leave other links as is, but move ahead of all processed
      '%' => array('mode' => 'link', 'weight' => -20),
    );
    $order = 0;
    $return = array();
    foreach ($settings as $key => $options) {
      $key = preg_replace('/[%]/', '.*', $key);
      $key = preg_replace('/!/', '\!', $key);
//FIXME: are we leaving old $key element behind? Should we remove it?
      $return[$key] = $options;
      $return[$key] += array('order' => $order, 'mode' => 'link');
      $order++;
    }
//watchdog('debug','preprocess_links settings=<pre>'.htmlentities(print_r($return,1)).'</pre>');
  }
  return $return;
}

/* 
 * Find settings record that matches given link.
 */
function _itweak_links_preprocess_links_match($settings, $link_key, $link, $type=NULL, $node_type=NULL) {
  // First try the fastest hash method - exact match
  if (isset($settings[$link_key])
    && (!isset($settings[$link_key]['key']) || $settings[$link_key]['key'] == 'key')
    && (!$type || !isset($settings[$link_key]['type']) || $settings[$link_key]['type'] == $type)
    && (!$node_type || !isset($settings[$link_key]['node_type']) || $settings[$link_key]['node_type'] == $node_type)
  ) {
    return $settings[$link_key];
  }

  // If exact match not found - scan through for a pattern match
  foreach ($settings as $key => $options) {
    if ($type && isset($options['type']) && $options['type'] != $type
      || $node_type && isset($options['node_type']) && $options['node_type'] != $node_type
    )
      continue;
    $needle = $link_key;
    if (isset($options['key']) && $options['key'] != 'key') {
      if (!isset($link[$options['key']])) return '';
      $needle = $link[$options['key']];
    }
    if ($key == $needle) return $options;
    if (preg_match('!^' . $key . '!', $needle)) return $options;
  }
  return '';
}

/**
 * @param $links
 *  Array of links to preprocess
 * @param $type
 *  Type of node to process the links for: 
 *  'node'        - standard node links
 *  'nodecomment' - standard node links on a nodecomment
 *  'comment'     - standard comment links
 *  'primary'     - advanced forum primary links
 *  'secondary'   - advanced forum secondary links
 */
function _itweak_links_preprocess_links(&$links, $type=NULL, $node_type=NULL) {
  if (!is_array($links)) return;
  if (isset($links['_itl_processed'])) return $links;    // Avoid double processing

  $settings = _itweak_links_preprocess_links_settings();

//watchdog('debug','preprocess_links=<pre>'.htmlentities(print_r($links,1)).'</pre>');
  $new_links = array();  // Create new array of links so we can change/append class names (keys)
  $weight = 0;
  $dropdowns = array();
  foreach ($links as $class => $link) {
    $weight++;
    $options = _itweak_links_preprocess_links_match($settings, $class, $link, $type, $node_type);
    if (is_array($options)) {
      if ($options['mode'] == 'none') {
        // Remove the link
        continue;  // foreach
      }
      if (isset($options['title'])) {
        // Override title
        $link['title'] = $options['title'];
      }
      $link_class = '';
      if (isset($options['class_replace'])) {
        $link_class = $options['class_replace'];
        $class = $link_class;
        $link['class'] = $link_class;
      }
      else if (isset($options['class'])) {
        $link_class = $options['class'];
        $class .= ' ' . $link_class;
        if (isset($link['class'])) $link['class'] .= ' ' . $link_class;
        else $link['class'] = $link_class;
      }
      if (isset($options['prefix'])) {
        $link['title'] = $options['prefix'] . $link['title'];
        $link['html'] = TRUE;
      }
      if (isset($options['suffix'])) {
        $link['title'] = $link['title'] . $options['suffix'];
        $link['html'] = TRUE;
      }
//      if ($options['mode'] == 'cssbutton') {
//        $span_class = ($link_class != '') ? $link_class : 'itweak-links-cssbutton';
//        // Add extra span tags for image replacement.
//        $link['title'] .= '<span class="' . $span_class . '"></span>';
//        $link['html'] = TRUE;
//      }
      if ($options['mode'] == 'dropdown') {
        $dropdown_class = isset($options['dropdown_class']) ? $options['dropdown_class'] : 'itweak-links-dropdown';
        $dropdown_name = isset($options['dropdown_name']) ? $options['dropdown_name'] : 'dropdown';

//?        $link_name = isset($options['name']) ? $options['name'] : '';

        if (!isset($dropdowns[$dropdown_name])) {
          $dropdowns[$dropdown_name] = array(
            'weight' => $options['order'],  // Weight among other links - just use the link's order since it is first declaration of the dropdown
            'order'  => $options['order'],  // also copy order for second-pass sorting
              // FIXME: i'm somewhat fuzzy on the weight/order here... I feel like one is not needed here
            '#weight' => 0,  // saved weight for link ordering within this dropdown (equivalent to $weight)
            '#class' => $dropdown_class,
          );
        }
        else {
          $dropdowns[$dropdown_name]['#weight']++;
        }
      }
      if (isset($options['weight'])) {
        // Override weight
        $link['weight'] = $options['weight'];
      } else if (isset($options['order'])) {
        // Save order value for second-pass reordering
        $link['order'] = $options['order'];
        if ($options['mode'] == 'dropdown') {
          // Ordering will be withing the dropdown
          $link += array('weight' => $dropdowns[$dropdown_name]['#weight']);
          $dropdowns[$dropdown_name]['#weight'] = $link['weight'];
        }
        else {
          $link += array('weight' => $weight);
          $weight = $link['weight'];
        }
      }
      if ($options['mode'] == 'dropdown') {
        $dropdowns[$dropdown_name][$class] = $link;
        continue; // foreach
      }
    } else {
      // Default handling for links not found in the settings
      $link += array('weight' => $weight);
      $weight = $link['weight'];
    }
    $new_links[$class] = $link;
  } // foreach
  
  // Now add the dropdowns in
  $classes_added = array(); // track what classes added to $new_links (avoids overwriting)
  foreach ($dropdowns as $dropdown_name => $dropdown) {
    $dropdown_weight = $dropdown['weight'];
    $dropdown_order = $dropdown['weight'];
    $dropdown_class = $dropdown['#class'];
    unset($dropdown['weight'], $dropdown['#weight'], $dropdown['#class'], $dropdown['order']);
    // Sort links within the dropdown
    uasort($dropdown, '_itweak_links_link_cmp');
    $html = theme('links', $dropdown, array('class' => $dropdown_class));
      // This generates ul list
    //FIXME: Build drop-down markup structure here:
    $html ='<span class="' . $dropdown_class . '-title"><a href="#" onclick="javascript: return false;">' . $dropdown_name . '</a></span>' . $html;    // Add dropdown title
    $i = 0;
    $check = $dropdown_class;
    while (in_array($check, $classes_added)) {
      $check = $dropdown_class . ' i' . $i++; // Add increment class
    }
    $dropdown_class = $check;
    $new_links[$dropdown_class] = array(
      'weight' => $dropdown_weight,
      'order' => $dropdown_order,
      'html' => TRUE,
      'title' => $html,
    );
    $classes_added[] = $dropdown_class;
  }
  
  // $weight value is preserved
  $weight++;
  foreach ($new_links as $key => $link) {
    if (isset($link['order'])) {
      $new_links[$key]['weight'] = $weight + $link['order'];
      unset($new_links[$key]['order']);
    }
  }

  uasort($new_links, '_itweak_links_link_cmp');
/*
watchdog('debug','OUT preprocess_links(' . $type .')'
.'<table><tr>'
.'<tr><th>Input<th>Output</tr>'
.'<td>Total '.count($links).':<pre>'.htmlentities(print_r($links,1)).'</pre>'
.'<td>Total '.count($new_links).':<pre>'.htmlentities(print_r($new_links,1)).'</pre>'
.'</tr></table>'
); */
  $links = $new_links;
  $links['_itl_processed'] = TRUE;
}

// Changed from _advanced_forum_preprocess_links_format_one($link, $type, $class) {
function _itweak_links_preprocess_links_format_one($link, $class, $type=NULL, $node_type=NULL) {
//watchdog('debug','process_one IN link=<pre>'.htmlentities(print_r($link,1)).'</pre>');
  if (!empty($link->href)) {
    $links[$class] = (array)$link;
//watchdog('debug','links BEFORE=<pre>'.htmlentities(print_r($links,1)).'</pre>');
    _itweak_links_preprocess_links($links, $type, $node_type);
//watchdog('debug','links AFTER=<pre>'.htmlentities(print_r($links,1)).'</pre>');
    foreach ($links as $class => $new_link) {
      if (isset($new_link['class'])) {
        // Replace given class with the override
        $class = $new_link['class'];
      }
      // Return the first one from the array.
      // We are not loosing anything, since _itweak_links_preprocess_links() is not creating new links
      $new_link['attributes']['class'] = $class;
      $link->link = l($new_link['title'], $new_link['href'], array(
       'fragment' => $new_link['fragment'],
       'html' => $new_link['html'],
       'attributes' => $new_link['attributes']));
    }
//watchdog('debug','process_one OUT link=<pre>'.htmlentities(print_r($link,1)).'</pre>');
  }
  return $link;
}

//END