<?php

/**
 * @file
 * Implementation of itweak_links administration forms.
 */

/**
 * Module settings form.
 */
function _itweak_links_admin_settings(&$form_state) {
  $form = array();
  
  $form['itweak_links_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fieldset'),
    '#description' => t('Some settings'),
  );

  $form['itweak_links_fieldset']['itweak_links_blanksetting'] = array(
    '#type' => 'checkbox',
    '#title' => t('Some setting'),
    '#default_value' => variable_get('itweak_links_blanksetting', TRUE),
    '#description' => t('This option makes some setting'),
  );


  return $form;
}
function itweak_links_admin_settings(&$form_state) {
  return system_settings_form(_itweak_links_admin_settings($form_state));
}

