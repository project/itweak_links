
iTweak Links
------------
by Ilya Ivanchenko, iva2k@yahoo.com

Many Drupal modules add their links to the so called "link" area that follows each node and each comment. Link area usually contains such items as "reply", "subscribe", "bookmark", etc. With many modules the links become crowded, cluttered and disorganized.

With iTweak Links you can beautify the links area. iTweak Links module can:
* remove some links
* reorder links
* group selected links into drop-down submenus
* change selected links title, class
* can turn links into buttons (customized by user theme or using included iTweak Links CSS)

The module is currently at "Proof of Concept" stage. There is no configuration GUI. All settings are hard-coded in the module's code.


Installation 
------------
* Copy the module's directory to your modules directory 
* Activate the module
 
Usage
-----
* Module starts working once enabled
* There are no configurable options


Credits
-------
Originally proposed by iva2k in one of Advanced Forum issues.

